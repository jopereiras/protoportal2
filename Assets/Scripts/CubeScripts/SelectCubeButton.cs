﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Button on Screen to un/select the cube to move by the player
/// it needs acces to the input manager to works
/// every time the player press this button the InputManager execute "SwitchToPortal"
/// </summary>
public class SelectCubeButton : MonoBehaviour
{
    ///  Input controller
    InputManager i_manager;
    /// Button Component of this gameObject
    Button button;
    /// Is the portal input enabled when game starts?
    bool isPortalFirst;

    /// Colour when button is selected (can move the cube)
    /// Colour when is not selected (can't move the cube)
    public Color selectedColour = Color.green;
    public Color unselectedColour = Color.red;
    
    /// GetComponents
    private void Awake()
    {
        button = GetComponent<Button>();
        i_manager = FindObjectOfType<InputManager>();
    }

    /// <summary>
    /// If there's no InputManager it through an error,
    /// if not OnButtonClick is added to execute when the button is pressed in the game.
    /// Change the colour acording to the initial input value on InputManager
    /// </summary>
    private void Start()
    {
        if(i_manager == null)
        {
            Debug.LogError("ADD A INPUT MANAGER TO A PERMANENT GAMEOBJECT IN SCENE");
            return;
        }
        isPortalFirst = i_manager.canInteractPortal;
        button.onClick.AddListener(delegate () { this.OnButtonClick(); });

        button.image.color = (isPortalFirst) ? unselectedColour : selectedColour;
    }

    /// <summary>
    /// Change the colour of the cube if is selected or not
    /// Execute SwitchToPortal from InputManager with isPortalFirst as an argument.
    /// </summary>
    void OnButtonClick()
    {
        isPortalFirst = !isPortalFirst;
        button.image.color = (isPortalFirst) ? unselectedColour : selectedColour;

        i_manager.SwitchToPortal(isPortalFirst);
        print("SWITCH TO PORTAL " + isPortalFirst);
    }
}
