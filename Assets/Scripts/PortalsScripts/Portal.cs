﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// It does the teleport thing! 
// Also contain the acceleration force when an Rigidbody come out from here
public class Portal : MonoBehaviour
{
    // Collider of the tile that is bellow this portal
    // the idea is to disable this collider, so any object can come through it
    Collider _targetCollider;
    public Collider targetCollider
    {
        get
        {
            return _targetCollider;
        }
        set
        {
            // Enable any older collider if a new one is set
            if(_targetCollider != null)
            {
                _targetCollider.enabled = true;
            }
            _targetCollider = value;
            // Disable the new collider if the Portal is active
            // thus an object can come through it and be teleported
            if(isActive)
            {
                value.enabled = false;
            }
        }
    }

    // A force added to the rigidbody when is teleported
    public float acceleration = 2F;

    // The target portal where to teleport;
    [HideInInspector] public Portal otherPortal;

    // Is this portal active?
    bool _isActive = false;
    public bool isActive
    {
        get
        {
            //return (otherPortal != null) ? true : false;
            return _isActive;
        }
        // Show an error if otherPortal is null
        // if is not then it disable the targetCollider and enable the entryCollider
        set
        {
            if(otherPortal == null)
            {
                Debug.LogError(this.gameObject.name + " Can't be activated, because there's no a destination Portal");
                _isActive = false;
                return;
            }

            entryTeleportColl.enabled = value;
            PortalReflectionMaterial p_reflect = entryTeleportColl.GetComponent<PortalReflectionMaterial>();
            p_reflect.exitPoint = otherPortal.exitTeleportPoint;
            p_reflect.allowReflection = true;
           
            targetCollider.enabled = !value;
            _isActive = value;
        }
    }
    [SerializeField] BoxCollider entryTeleportColl;

    [SerializeField] Transform _exitTeleportPoint;
    public Transform exitTeleportPoint
    {
        get
        {
            return _exitTeleportPoint;
        }
    }

    private void Awake()
    {
        entryTeleportColl.enabled = false;
    }

    public void Teleport(Rigidbody rig)
    {
        Vector3 vel = rig.velocity;
        vel = new Vector3(0, 0, vel.y);
        rig.position = otherPortal.exitTeleportPoint.position;
        rig.rotation = otherPortal._exitTeleportPoint.rotation;
        rig.velocity = Vector3.zero;
        rig.AddRelativeForce(Vector3.forward * (vel.magnitude + acceleration), ForceMode.Impulse);
    }
}
