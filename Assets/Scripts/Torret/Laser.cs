﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Laser : MonoBehaviour
{
    public int nReflections = 10;
    LineRenderer laserLine;
    RaycastHit hit;
    public Vector3 dir;
    public Vector3 from;
    ReflexionMaterial reflectMat;

    // 8 = Portal
    // 9 = Portal Object 
    // 10 = Wall
    // 11 = InvisibleWall
    int layerMask = ((1 << 8) | (1 << 9) | (1 << 10) | (1 << 11));

    // Laser Refelcted by a Portal
    public Laser otherLaser;

    private void Awake()
    {
        laserLine = GetComponent<LineRenderer>();
    }
    private void Start()
    {
        laserLine.enabled = false;
    }

    private void Update()
    {
        laserLine.SetPosition(0, transform.position);
        for(int i = 0; i < nReflections; i++)
        {
            if (i == 0)
            {
                from = transform.position;
                dir = transform.forward;            
            }
            if (Physics.Raycast(origin: from, direction: dir, hitInfo: out hit, maxDistance: 50F, layerMask: layerMask))
            {
                reflectMat = hit.transform.GetComponent<ReflexionMaterial>();
                if (reflectMat != null)
                {
                    bool isPortal = reflectMat.GetType().IsSubclassOf(typeof(ReflexionMaterial));
                    if (isPortal)
                    {
                        if(otherLaser == null)
                        {
                            print("REFLECT ON " + reflectMat.exitPoint.parent.name);
                            otherLaser = Instantiate(GamePlatform.instance.LaserRefence, reflectMat.exitPoint.position, Quaternion.identity).GetComponent<Laser>();
                            otherLaser.transform.parent = reflectMat.exitPoint.transform;
                        }
                        otherLaser.transform.localRotation = Quaternion.identity;
                    }
                    else
                    {
                        from = hit.point;
                        dir = Vector3.Reflect(dir, reflectMat.exitPoint.forward);

                        // REFLECT WITH 45 DEGRESS DIRECTION
                        dir.x = Mathf.Round(dir.x * 2F) / 2f;
                        dir.y = Mathf.Round(dir.y * 2F) / 2f;
                        dir.z = Mathf.Round(dir.z * 2F) / 2f;
                    }

                }
                else
                {
                    if(otherLaser != null)
                    {
                        GameObject.Destroy(otherLaser.gameObject);
                    }
                    laserLine.enabled = true;
                    laserLine.positionCount = i + 2;
                    laserLine.SetPosition(i + 1, hit.point);
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }

    private void OnDestroy()
    {
        if (otherLaser != null)
        {
            GameObject.Destroy(otherLaser.gameObject);
        }
    }
}
