﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SwipeCubeModel))]
public class CubeOnEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SwipeCubeModel controller = (SwipeCubeModel)target;

        if(GUILayout.Button("ROTATE"))
        {
            controller.Rotate(controller.dirAxis);
        }

    }
}
