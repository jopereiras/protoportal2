﻿using UnityEngine;
using System.Collections;

public class PortalReflectionMaterial : ReflexionMaterial
{
    Portal portal;

    private void Awake()
    {
        portal = GetComponentInParent<Portal>();
        allowReflection = false;
    }
}
