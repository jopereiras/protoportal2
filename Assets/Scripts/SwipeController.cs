﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// DECRAPITATED
/// it will be deleted after testing
/// do not use it: use InputManager instead
public class SwipeController : MonoBehaviour
{
    Touch fingerTouch;

    [Range(5, 100)] [SerializeField] float swipeInverseSensibility = 50F;
    [SerializeField] Vector2 _onScreemDirection;
    public Vector2 onScreemDirection
    {
        get
        {
            return _onScreemDirection;
        }
    }
    
    bool hasMoved = false;
    public Vector2 initialPos
    {
        get; private set;
    }
    public Vector2 finalPos
    {
        get; private set;
    }

    [SerializeField] float resetTime = 0.3F;
    public bool canInteract = false;

    private void Update()
    {
        if(!canInteract)
        {
            return;
        }
        if(Input.touchCount > 0)
        {
            fingerTouch = Input.GetTouch(0);
            switch (fingerTouch.phase)
            {
                case TouchPhase.Began:
                    _onScreemDirection = Vector2.zero;
                    initialPos = Vector2.zero;
                    finalPos = Vector2.zero;
                    _onScreemDirection = Vector2.zero;
                    initialPos = fingerTouch.position;
                    break;

                case TouchPhase.Moved:
                    hasMoved = true;
                    break;

                case TouchPhase.Stationary:
                case TouchPhase.Ended:
                    float moveSpeed = fingerTouch.deltaPosition.magnitude / Time.deltaTime;
                    if (!hasMoved || (moveSpeed < swipeInverseSensibility))
                    {
                        //initialPos = Vector2.zero;
                        //finalPos = Vector2.zero;
                        _onScreemDirection = Vector2.zero;
                        return;
                    }

                    finalPos = fingerTouch.position;
                    _onScreemDirection = new Vector2(finalPos.x - initialPos.x, finalPos.y - initialPos.y).normalized;
                    _onScreemDirection.x = Mathf.Round(_onScreemDirection.x);
                    _onScreemDirection.y = Mathf.Round(_onScreemDirection.y);

                    hasMoved = false;
                    StartCoroutine(WaitToResetDirection());
                    break;

                case TouchPhase.Canceled:
                    hasMoved = false;
                    _onScreemDirection = Vector2.zero;
                    break;
            }
        }
    }

    IEnumerator WaitToResetDirection()
    {
        canInteract = false;
        yield return new WaitForSeconds(resetTime);
        _onScreemDirection = Vector2.zero;
        canInteract = true;
    }
}
