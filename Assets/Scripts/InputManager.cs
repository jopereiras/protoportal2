﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contain all the input during the main game scene:
/// PortalCreation and SwipeDirection
/// The user can switch beetwen them
/// It doesn't apply to the main menu or pause menu
/// </summary>
public class InputManager : MonoBehaviour
{
    /// <summary>
    /// Event when the Swipe is enable. 
    /// The CubeController or other corresponding object may respond to this
    /// </summary>
    /// <param name="value"></param>
    public delegate void SwipeEnabledEvenHandler(bool value);
    public event SwipeEnabledEvenHandler OnSwipeEnabled;

    /// Bool to switch beetwen Portal and Swipe Input.
    /// For setting porpuse use SwitchToPortal(bool value)
    [Header("Inital Input: Select 1", order = 0)]
    public bool canInteractPortal;
    public bool canInteractSwipe;

    #region PORTAL PROPERTIES
    [Header("Portal Properties", order = 1)]
    /// The Portal Controller, contain the reference of the portals
    public PortalController c_portal;
    /// The target layerMask to place portals
    public LayerMask targetLayer;
    /// MainCamera reference
    Camera cam;
    /// Is the player tapping on a ableObject?
    /// hit check if the collision from the touchPosition
    /// is colling with an object in the target layer
    RaycastHit hit;
    /// Will the first (orange) or second (blue) portal be created?
    /// indexPortal loop from 0 to 1
    /// use the public property here too. 
    int _indexPortalToCreate = 0;
    public int indexPortalToCreate
    {
        get
        {
            return _indexPortalToCreate;
        }
        private set
        {
            if(value < 0)
            {
                value = 1;
            }
            else if(value > 1)
            {
                value = 0;
            }
            _indexPortalToCreate = value;
        }
    }
    #endregion

    #region SWIPE PROPERTIES
    [Header("Swipe Properties", order = 2)]

    /// The finger reference
    Touch fingerTouch;
    /// Sensibility of the swipe detection
    /// use biggers values for less sensibility 
    /// small values for most sensibilty
    /// 20 could well.
    [Range(5, 100)] public float swipeInverseSensibility = 20F;
    /// Time (in seconds) to allow a new swipe 
    public float resetTime = 0.2F;

    /// <summary>
    /// Output: Direction of swipe (normalized vector2).
    /// </summary>
    Vector2 _swipeDirection;
    public Vector2 swipeDirection
    {
        get
        {
            return _swipeDirection;
        }
    }

    /// <summary>
    /// Has the player's finger move?
    /// </summary>
    bool hasMoved = false;

    /// Initial player's finger position (in Screen Coordinates)
    public Vector2 initialPos
    {
        get; private set;
    }

    /// Final player's finger position, if he has move it (in Screen Coordinates)
    public Vector2 finalPos
    {
        get; private set;
    }

    #endregion

    private void Awake()
    {
        cam = Camera.main;
    }

    /// <summary>
    /// Initialize PortalController if it has been seted from Editor
    /// SwitchPortal() with initial value from editor.
    /// </summary>
    private void Start()
    {
        if(c_portal.portaReference.Length > 0)
        {
            PortalController temp_controller = c_portal;
            c_portal = new PortalController(temp_controller.portaReference);
        }

        SwitchToPortal(canInteractPortal);
    }

    private void Update()
    {
        // Portal Control
        if(canInteractPortal && Input.GetMouseButtonDown(0))
        {
            /// Cast a ray from the Camera
            /// if it hits on a object then Create a Portal in that point
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            ray.direction = cam.transform.forward;
            if (!Physics.Raycast(ray, out hit, 100F, targetLayer))
            {
                return;
            }

            CreatePortal(indexPortalToCreate, hit);
        }

        /// Swipe Control
        if (canInteractSwipe && Input.touchCount > 0)
        {
            fingerTouch = Input.GetTouch(0);
            switch (fingerTouch.phase)
            {
                case TouchPhase.Began:
                    _swipeDirection = Vector2.zero;
                    initialPos = Vector2.zero;
                    finalPos = Vector2.zero;
                    _swipeDirection = Vector2.zero;
                    initialPos = fingerTouch.position;
                    break;

                case TouchPhase.Moved:
                    hasMoved = true;
                    break;

                case TouchPhase.Stationary:
                case TouchPhase.Ended:
                    float moveSpeed = fingerTouch.deltaPosition.magnitude / Time.deltaTime;
                    if (!hasMoved || (moveSpeed < swipeInverseSensibility))
                    {
                        _swipeDirection = Vector2.zero;
                        return;
                    }

                    finalPos = fingerTouch.position;
                    _swipeDirection = new Vector2(finalPos.x - initialPos.x, finalPos.y - initialPos.y).normalized;
                    _swipeDirection.x = Mathf.Round(_swipeDirection.x);
                    _swipeDirection.y = Mathf.Round(_swipeDirection.y);

                    hasMoved = false;
                    StartCoroutine(WaitToResetDirection());
                    break;

                case TouchPhase.Canceled:
                    hasMoved = false;
                    _swipeDirection = Vector2.zero;
                    break;
            }
        }
    }
        
    // Instatiate or Move a Portal by its inde  x
    // NOTE: Quateriorn.LookRotation(hit.normal) is use to face the portal angle with the collider Platform angle
    public void CreatePortal(int index, RaycastHit hit)
    {
        Portal p;
        Vector3 dir = (hit.transform.tag == "FrontWall") ? -(hit.normal * 0.06F) : hit.normal * 0.06F;
        if (c_portal.instantiatedPortals[index] == null)
        {
            p = Instantiate(c_portal.portaReference[index], hit.transform.position + (dir), Quaternion.LookRotation(dir));
            p.targetCollider = hit.collider;
            c_portal.SetPortalAt(index, p);
        }
        else
        {
            p = c_portal.instantiatedPortals[index];
            p.targetCollider = hit.collider;
            p.transform.position = hit.transform.position + (dir);
            p.transform.rotation = Quaternion.LookRotation(dir);
        }
    }

    //USE FOR BUTTONS ON INSPECTOR
    // Change the index in order to create portals by clicing or taping
    public void ChangePortalIndex(int index)
    {
        if(!canInteractPortal)
        {
            SwitchToPortal(true);
        }
        indexPortalToCreate = index;
    }

    IEnumerator WaitToResetDirection()
    {
        canInteractSwipe = false;
        yield return new WaitForSeconds(resetTime);
        _swipeDirection = Vector2.zero;
        canInteractSwipe = true;
    }

    public void SwitchToPortal(bool value)
    {
        canInteractPortal = value;
        canInteractSwipe = !value;

        if(OnSwipeEnabled != null)
        {
            OnSwipeEnabled(canInteractSwipe);
        }
    }
}
