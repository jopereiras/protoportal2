﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Struct that contain the portal's reference to instantiate them
/// and a reference of the portals that are created in the scene
/// Use SetPortalAt(index, portal) to set the value of an instantiated portal in game,
/// thus it can check to active the teleporting.
/// </summary>
[System.Serializable]
public struct PortalController
{
    /// The idea is only use 2 portals
    /// Array of reference set in inspector
    [SerializeField] Portal[] _portalReference;
    /// Array (readOnly) use in code.
    public Portal[] portaReference
    {
        get
        {
            return _portalReference;
        }
    }
    /// Reference of portals in Scene (for set use SetPortalAt)
    public Portal[] instantiatedPortals;


    /// if both Portal are in game, this function activate them.
    public void SetPortalAt(int index, Portal value)
    {
        instantiatedPortals[index] = value;
        if (instantiatedPortals[0] != null && instantiatedPortals[1] != null)
        {
            instantiatedPortals[0].otherPortal = instantiatedPortals[1];
            instantiatedPortals[1].otherPortal = instantiatedPortals[0];

            instantiatedPortals[1].isActive = true;
            instantiatedPortals[0].isActive = true;
        }
    }

    /// Contructor with separete portal reference,
    /// and also a contructor with a array of portals
    public PortalController(Portal first, Portal second)
    {
        instantiatedPortals = new Portal[2];
        _portalReference = new Portal[2];
        _portalReference[0] = first;
        _portalReference[1] = second;
    }
    public PortalController(Portal[] portalsReference)
    {
        if(portalsReference.Length < 2)
        {
            Debug.LogError("CANNOT CREATE LESS THAN 2 PORTALS");
        }

        instantiatedPortals = new Portal[2];
        _portalReference = new Portal[] { portalsReference[0], portalsReference[1] };
    }
}
