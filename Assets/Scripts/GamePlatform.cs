﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains the data for rendering the platforms (initialWalls) in the scene
/// as this is a prototype this class would also works as a global object reference(for future intantiation)
/// for that last porpuse use the singleton instance
/// </summary>

public class GamePlatform : MonoBehaviour
{
    // Singleton for references
    public static GamePlatform instance;
    // Reference to lasers object (use it to create laser in portals)
    public GameObject LaserRefence;

    [Header("Transforms")]
    // References for walls in game
    [SerializeField] Transform[] frontWalls; // Invisibles when game starts
    [SerializeField] Transform[] backWalls; // Visibles when game starts

    // Color when the front walls are visibles
    [Header ("Colours FrontWall Enabled")]
    [SerializeField] Color selectedFrontColour = Color.white;

    // Layer from selected (visible) walls and unselected walls
    // use them to set the interaction with the player (create portals)
    [Header("LayerMasks Choose one value")]
    [SerializeField] LayerMask selectedLayer;
    [SerializeField] LayerMask unselectedLayer;
    // The integres values for the layers above
    int intSelectedLayer;
    int intUnselectedLayer;

    // The sprite when the walls is not interactable (unselected)
    [Header("Sprites")]
    [SerializeField] Sprite unselectedSprite;

    /// All the SpriteRenderes in all the wall's Childs
    /// The same for sprites
    /// The first array correspond to the wall transform reference 
    /// the second array are the data in that transform
    SpriteRenderer[][] rendData;
    Sprite[][] spritesData;

    // Variable to switch and get the wall's state
    bool _selectFront = false;
    public bool selectFront
    {
        get
        {
            return _selectFront;
        }
        set
        {
            if (value)
            {
                //print("SHOW FRONT");
                SelectFrontWalls();
            }
            else
            {
                //print("SHOW BACK");
                SelectBackWalls();
            }
            _selectFront = value;
        }
    }

    /// <summary>
    /// Initialize Singleton
    /// Change the layers from LayerMask to int
    /// Execute GetData()
    /// </summary>
    private void Awake()
    {
        instance = this;

        // Active the front walls (if there're not active in scene)
        for (int i = 0; i < frontWalls.Length; i++)
        {
            frontWalls[i].gameObject.SetActive(true);
        }
        intUnselectedLayer = (int)Mathf.Log(unselectedLayer.value, 2);
        intSelectedLayer = (int)Mathf.Log(selectedLayer.value, 2);        

        GetData();
    }

    /// <summary>
    /// Execute SwtichWall()
    /// </summary>
    private void Start()
    {
        SwitchWall();
    }

    /// <summary>
    /// Method use by the button on UI to switch the state of walls
    /// </summary>
    public void SwitchWall()
    {
        selectFront = !selectFront;
    }

    /// <summary>
    /// Collect all the spriteRenderers and all initial sprites from every wall's tile
    /// Save that data in the rendData and spriteData respectively
    /// </summary>
    private void GetData()
    {

        spritesData = new Sprite[frontWalls.Length + backWalls.Length][];
        rendData = new SpriteRenderer[frontWalls.Length + backWalls.Length][];

        /// Each sprite and spriteRenderer of the current wall is save here
        /// then is set as the spriteData and rendData value
        Sprite[] tempSprArr;
        SpriteRenderer[] tempRendArr;

        /// First save the data from the frontWalls
        /// Thus the first index on the data arrays are from frontWalls

        /// Iterate for each front wall
        for (int i = 0; i < frontWalls.Length; i++)
        {
            int childCount = frontWalls[i].childCount;
            tempRendArr = new SpriteRenderer[childCount];
            tempSprArr = new Sprite[childCount];

            // iterate for each child in the curent wall
            for (int j = 0; j < childCount; j++)
            {
                tempRendArr[j] = frontWalls[i].GetChild(j).GetComponent<SpriteRenderer>();
                tempSprArr[j] = tempRendArr[j].sprite;
            }
            /// Set the rend and sprite data with the current wall index
            /// with the temp array
            /// do it for each front wall
            rendData[i] = tempRendArr;
            spritesData[i] = tempSprArr;
        }

        /// Iterate for each backWall
        /// as the "i" variable starts as the length of the frontWalls array
        /// the we can't iterate for each backWall object using "i",
        /// that's why there's "index" variable with the initial value of 0
        /// index increase by 1 for each iteration.
        int index = 0;
        /// use "index" to get the child from the current back walls
        /// use "i" and "j" to set the values on the final array of data
        for (int i = frontWalls.Length; i < rendData.Length; i++, index++)
        {
            int childCound = backWalls[index].childCount;
            tempRendArr = new SpriteRenderer[childCound];
            tempSprArr = new Sprite[childCound];

            /// iterate for each child on the current back wall
            for (int j = 0; j < childCound; j++)
            {
                tempRendArr[j] = backWalls[index].GetChild(j).GetComponent<SpriteRenderer>();
                tempSprArr[j] = tempRendArr[j].sprite;
            }

            /// Set the rend and sprite data with the current wall index
            /// with the temp array
            /// do it for each front wall
            rendData[i] = tempRendArr;
            spritesData[i] = tempSprArr;
        }
    }


    /// <summary>
    /// Make back walls visible (i >= frontWalls.length)
    /// and make front walls invisible (with the unselected sprite)
    /// the selected colour doesn't apply here, due to the back walls does not interrupt to player's view
    /// </summary>
    void SelectBackWalls()
    {
        int temp_length;
        for (int i = 0; i < rendData.Length; i++)
        {
            temp_length = rendData[i].Length;
            if (i >= frontWalls.Length)
            {
                for (int j = 0; j < temp_length; j++)
                {
                    rendData[i][j].sprite = unselectedSprite;
                    rendData[i][j].gameObject.layer = intUnselectedLayer;
                }
            }
            else
            {
                for (int j = 0; j < temp_length; j++)
                {
                    rendData[i][j].sprite = spritesData[i][j];
                    rendData[i][j].gameObject.layer = intSelectedLayer;
                }
            }
        }
    }

    /// <summary>
    /// Select the FrontWalls (i < frontWalls.lenght)
    /// These have the selected colour on its sprite because the interception with the player's view
    /// the back wall's tiles have the unselected (wired) sprite.
    /// </summary>
    void SelectFrontWalls()
    {
        int temp_length;
        for (int i = 0; i < rendData.Length; i++)
        {
            temp_length = rendData[i].Length;
            if (i < frontWalls.Length)
            {
                for (int j = 0; j < temp_length; j++)
                {
                    rendData[i][j].sprite = unselectedSprite;
                    rendData[i][j].gameObject.layer = intUnselectedLayer;
                    rendData[i][j].color = selectedFrontColour;
                }
            }
            else
            {
                for (int j = 0; j < temp_length; j++)
                {
                    rendData[i][j].sprite = spritesData[i][j];
                    rendData[i][j].gameObject.layer = intSelectedLayer;
                }
            }
        }
    }
}
