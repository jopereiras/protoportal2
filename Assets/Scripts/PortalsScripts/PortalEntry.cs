﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEntry : MonoBehaviour
{
    // The parent Portal Component
    Portal portal;

    private void Start()
    {
        portal = GetComponentInParent<Portal>();
    }

    // If any rigidbody enters here, then Teleporte it
    private void OnTriggerEnter(Collider collision)
    {
        Rigidbody rig = collision.GetComponent<Rigidbody>();
        portal.Teleport(rig);
    }

}
