﻿using UnityEngine;

namespace MathTools
{
    public static class Tools
    {
        public static float ReflectedAngle(Vector3 incidnetRay, Vector3 normal)
        {
            float cos = Vector3.Dot(incidnetRay, normal) / (incidnetRay.magnitude * normal.magnitude);
            float angle = (Mathf.Acos(cos) * (180f / Mathf.PI));
            return Mathf.Abs((180-angle) * 2);
        }

        public static Vector3 ConvertAngleToVector3(float degree)
        {
            float angleInRad = (degree -90) * Mathf.Deg2Rad;
            return new Vector3(Mathf.Cos(angleInRad), 0.0F, Mathf.Sin(angleInRad));
        }
    }
}
