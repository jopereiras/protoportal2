﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A controller to the SwipeCubeModel
/// Input: swipeDirection from InputManger
/// Process: Transform input into a Vector3 direcition
/// Output: Rotate the cubeModel with the fixed direction.
/// Select and Unselect the model Cube.
/// </summary>
[RequireComponent(typeof(SwipeCubeModel))]
public class SwipeCubeController : MonoBehaviour
{
    /// Reference to the Input
    InputManager i_manager;

    /// The model Cube. Must be on the same GameObject
    public SwipeCubeModel model
    {
        get; private set;
    }

    /// is the Cube selected.
    bool selectCube;
    public bool isCubeSelected
    {
        get
        {
            return selectCube;
        }
    }
    
    /// The final direction of movement
    Vector3 direction;


    private void Awake()
    {
        i_manager = FindObjectOfType<InputManager>();
        model = GetComponent<SwipeCubeModel>();

        i_manager.OnSwipeEnabled += SelectCube;
    }

    /// <summary>
    /// Return if the model can't rotate or is not selected
    /// Return if the direction is zero.
    /// Check the x and y direction from input and change them to x and z direction
    /// </summary>
    private void Update()
    {
        if(!selectCube || !model.canRotate)
        {
            direction = Vector3.zero;
            return;
        }
        direction = new Vector3(-i_manager.swipeDirection.x, 0, i_manager.swipeDirection.y);
        if(direction == Vector3.zero)
        {
            return;
        }
        if(direction.x > 0 && direction.z < 0)
        {
            direction.x = 0F;
        }
        else if (direction.x < 0 && direction.z > 0)
        {
            direction.x = 0F;
        }
        else if (direction.x > 0 && direction.z > 0)
        {
            direction.z = 0F;
        }
        else if (direction.x < 0 && direction.z < 0)
        {
            direction.z = 0F;
        }

        model.Rotate(direction);        
    }

    /// <summary>
    /// Select the model cube
    /// </summary>
    /// <param name="value"></param>
    public void SelectCube(bool value)
    {
        selectCube = value;
        model.SelectCube(value);
    }

}
