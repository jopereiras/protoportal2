﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This Cube only rotate from a pivot point with a given direction
/// Use this with SwipeCubeController
/// </summary>
public class SwipeCubeModel : MonoBehaviour
{
    // Direction of movement use by Editor
    [SerializeField] Vector3 _dirAxis;
    // Read Only direction
    public Vector3 dirAxis
    {
        get
        {
            return _dirAxis;
        }
    }

    // PivotPoint (We rotate this not the cube)
    Transform pivotPoint;
    // Speed of movement (with 5 works well)
    public float moveSpeed;
    // If the object is rotating or falling it cannot rotate
    public bool canRotate;
    // Time beetwen every rotation interaction
    [SerializeField] float watingTime = 1F;

    // The collider and rigidbody of this object
    BoxCollider coll;
    Rigidbody rig;

    /// Renderer
    Renderer rend;

    // GetComponents
    private void Awake()
    {
        coll = GetComponent<BoxCollider>();
        rig  = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
    }

    /// <summary>
    /// Normalize rotation
    /// If this object doesn't have a parent (pivot point) We create one.
    /// </summary>
    private void Start()
    {
        this.transform.rotation = Quaternion.identity;
        pivotPoint = (transform.parent != null) ? transform.parent : new GameObject("PivotPoint" + this.name).transform;
        pivotPoint.rotation = Quaternion.identity;
    }

    // If is falling it can not rotate
    private void Update()
    {
        if (rig.velocity.y < -1.5F * rig.mass)
        {
            canRotate = false;
        }
    }

    // If it lands for falling then it can rotate again
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.relativeVelocity.magnitude > 1.5F)
        {
            canRotate = true;
        }
    }

    /// PLACE PIVOT POINT IN THE CORRECT PLACE
    /// Initialize rotating movement
    /// axis = Vector3 with only one axis different that 0 (int values)
    public void Rotate(Vector3 axis)
    {
        /// Check if the axis param is correct
        /// return if it cannot rotate
        /// return if axs is equal to 0
        /// return if axis has more than one axis different than 0
        Vector3 absAxis = new Vector3(Mathf.Abs(axis.x), Mathf.Abs(axis.y), Mathf.Abs(axis.z));
        if (!canRotate || axis == Vector3.zero || (absAxis.x + absAxis.y + absAxis.z) > 1F)
        {
            return;
        }
        
        /// Normalize axis for direction
        /// it can't rotate from here
        canRotate = false;
        if (axis.magnitude > 1F)
        {
            axis = axis.normalized;
        }
        _dirAxis = axis;

        /// The atatch from the parent (so we only move the parent tranform)
        /// and place the parent transform in the downcorner corresponding to the rotatio axis
        this.transform.parent = null;
        pivotPoint.rotation = Quaternion.identity;
        pivotPoint.position = new Vector3
            (
                transform.position.x + ((coll.size.x / 2) * (axis.z )),
                transform.position.y - (coll.size.y / 2),
                transform.position.z + ((coll.size.z / 2) * (axis.x))
            );
        // Atatch the parent again
        this.transform.parent = pivotPoint;   
        // Start Rotation routine        
        StartCoroutine(WaitToRotate(axis));
    }

    /// ROTATE PIVOT HERE SMOOTHLY
    /// axis = direction of rotation
    IEnumerator WaitToRotate(Vector3 axis)
    {
        /// We disable physics to manipulte the Transform
        /// when the rotation is done, we enable physics again
        rig.isKinematic = true;

        /// The target rotation on Quaternion. Never use EulerAngle (is not presice)
        Quaternion newRotation = Quaternion.AngleAxis(90, new Vector3 (axis.x, axis.y, -axis.z));

        /// Interpolate between the initial rotation and the final rotation using Slerp
        while ( pivotPoint.rotation != newRotation)
        {
            pivotPoint.rotation = Quaternion.Slerp(pivotPoint.rotation, newRotation, Time.deltaTime * moveSpeed);
            yield return null;
        }
        /// Enalble physics
        /// Reset input direction.
        rig.isKinematic = false;
        _dirAxis = Vector3.zero;

        // Wait to rotate again
        yield return new WaitForSeconds(watingTime);
        canRotate = true;
    }

    /// <summary>
    /// Change the colour of this cube is selected or not.
    /// </summary>
    /// <param name="value"></param>
    public void SelectCube(bool value)
    {
        if (value)
        {
            rend.material.color = Color.red;
        }
        else
        {
            rend.material.color = Color.green;
        }
    }
}
